# **Практическая работа №6**
## Документирование кода

*Программа включает в себя две фунции: одна находит площадь пересечения двух прямоугольников, другая - площадь объединения.*

![intersection](http://pcabc.ru/paintnet/paint16image017.gif) *Пересечение двух прямоугольников* ~*картинку объединения не смог найти*~

## Пример кода

```python
def intersection(x1, y1, x2, y2, x3, y3, x4, y4):
    """
    Находит площадь пересечения двух прямоугольников, заданных 8 координатами.
    :param x1: Координата x левого верхнего угла 1-го прямоугольника
    :param y1: Координата y левого верхнего угла 1-го прямоугольника
    :param x2: Координата x правого нижнего угла 1-го прямоугольника
    :param y2: Координата y правого нижнего угла 1-го прямоугольника
    :param x3: Координата x левого верхнего угла 2-го прямоугольника
    :param y3: Координата y левого верхнего угла 2-го прямоугольника
    :param x4: Координата x правого нижнего угла 2-го прямоугольника
    :param y4: Координата y правого нижнего угла 2-го прямоугольника
    :return: Площадь кв.ед.
    """
    left = max(x1, x3)
    right = min(x2, x4)
    top = min(y1, y3)
    bottom = max(y2, y4)
    width = right - left
    height = top - bottom
    if width < 0 or height < 0:
        return 0
    else:
        return width * height
```

## Решение похожей задачи на C# 

Ссылка на [stackoverflow](https://ru.stackoverflow.com/questions/1100712/%D0%9F%D0%B5%D1%80%D0%B5%D1%81%D0%BA%D0%B0%D1%8E%D1%89%D0%B8%D0%B5%D1%81%D1%8F-%D0%BF%D1%80%D1%8F%D0%BC%D0%BE%D1%83%D0%B3%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%B8).

